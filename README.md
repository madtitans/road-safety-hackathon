# Road Safety Hackathon

## Table of Content
* [Introduction](#1---introduction)
* [Objectives](#2---objectives)
* [Features](#3---features)
* [Running The Project](#4----running-the-project)
    * [Permissions Required](#permissions-required-)
    * [Instructions For Direct APK Installation](#instructions-for-direct-apk-installation-)
    * [Instructions For Developers/Testers](#instructions-for-developerstesters-)
* [Technology Stack Used](#5---technology-stack-used)
    * [Front End Development](#front-end-development)
    * [Back End Development](#back-end-development)
    * [Frameworks](#frameworks)
    * [Packages](#packages)
* [Team's Area of Expertise](#6--teams-area-of-expertise)
* [Team](#7--team)

## 1.   Introduction
Our Solution will have two parts:

=>First is Automated data collection through Google APIs,  and will notify the concerned authority on their registered portals and social handles. We will collect data like livefeeds about the road conditions

=>Second part includes: Collecting data from the user. They can report the conditions of the roads via clicking the images and it'll populate the database and notify the authorities accordingly.

=>Additionally, we want to provide these services:

•       Engages common people of the area. A platform can be created which will have these additional features like allow the people of the area or the user of the roads to be able to upload an "Image" of the damaged road”.
•  The "Image" will have the location details automatically and an issue will be raised to the designated authority and then the assigned authorities will have a fixed number of days to resolve the issue at hand.
•  In case of the failure, the problem along with the picture will be automatically sent to the higher authority. 
•  After that it will be forwarded to the area head, to the city or the state head and so on according to the hierarchy set in case of continuous ignorance or failure
•  To make things more effective and transparent, all these issues will be available to general public via website, which can be accessed globally.

Our Objectives:
This idea could help people in many ways:
•  Elimination one of the main reasons of road accidents.
•  Keeping and maintaining the Driveways.
•  Eliminating delays/ignorance (authorities will have to complete their jobs in a time frame).
•  Responsible authorities will have to work with total transparency, hence, reducing the corruption problem.

The above collected data (automated+manual) will be cross verified before forwarding the data to the concerned authority

This Android application is built on .NET and Xamarin Framework , and hence, can also be ported across Major platforms, like iOS and Windows.

>You need at least Android API level 21 (Lollipop) to run this application.

> The libraries used in this project are mostly (if not all) Open Source and Free to use and distribute.We do not claim any copyright on the external libraries.


## 2.   Objectives
  - User Friendly Interface
    -    A more engaging and easy-to-use graphical user interface for all types of users, ranging from developers to non-technical sound users.
    -    Easy Login/Signup and pages to create and view New Records.

 - Creating New Records
    -    Application provides clear and detailed description on how to create new records by Users.
    -    Users can click the photos of the person and fill in the required information and submit it to the database.

## 3.   Features
This prototype application contains some of the Industry-standard features such as 

- Material Design.

- Faster Bootup.

- Easier Login and Signup.

- Ability to create New Records.

- Upload Records to Database in real-time.

- Near Real-time data update.

- List of all records available in One-go.

- Uses APIs to get the content in real time.


# 4.    Running The Project
Let  us remind you again that the minimum Android OS that you need to run this project is Lollipop (API Level 21). So, make sure you're satisfying the minimum requirements first. Otherwise, your handset won't be able to parse the apk file.

- ### Permissions Required :
    This application requires you to provide few permissions to it, in order to work properly. Here's the list of permissions that the application needs :
    - Internet Access
    - View WiFi Connections
    - Storage (Read/Write Perms For Cache)
    - Read Google Service Configuration
    - Camera (For Taking Pictures)

- #### Instructions For Direct APK Installation :
    If you want to run this application on your android phone, please move over to the "[`Downloads`](https://bitbucket.org/madtitans/road-safety-hackathon/downloads/)" section and download the latest stable APK build for your android phone. You do not need any external libraries or application.

- #### Instructions For Developers/Testers :
     If you're a developer or any user who wishes to test this application and run this android project, it's recommended to install Visual Studio with Xamarin Support and Android SDKs on your system. Remember that Android SDKs should be in your local path for you to be able to compile the project properly. You can find the source code in the "[SOURCE](https://bitbucket.org/madtitans/road-safety-hackathon/src)" directory.

    If you do not happen to have Visual Studio, it is recommended to get it because it'll download all the required packages on its own, if they're not present. You can use Visual Studio's Free Community Edition. It'll work, as we've developed this application on it.
But, if for some reason, you don't want to or can't install Visual Studio, you will need to have .NET, Xamarin, Android SDK and required Packages in your system's local path for you to be able to compile and execute this application project.


## 5.   Technology Stack Used
- ### Front End Development

    - ##### User Interface design:
       - Android XML (.axml)

    - ##### Other Softwares: 
       - GIMP

- ### Back End Development

    - ##### Main programming language:
       - C# - to develop base functionalities of proposed application.

    - ##### Basic scripting languages:
       - PHP - for Parsing back-end

-    ### Frameworks

       - Xamarin Framework-  to develop native portable Android Application in C#
       - .NET Framework-  to use Microsoft's Visual Studio and C# basic packages.

-    ### Packages

       - C# Basic Packages-  provided by Microsoft.
       - Newtonsoft -  To parse JSON data.
       - Google Play Services -  Packages provided by Google for Android Development.

## 6.  Team's Area of Expertise
- ### Front End Developer
    - **Ankit Passi** **(** UI/UX Designer **)**
        - Photoshop
        - GIMP
        - Illustrator
        - C#
        - C++
        - Xamarin Framework
        - VR Developer
        - Well-versed with Unreal Engine Visual Scripting.


- ### Back End Developers
    - **Dhruv Kanojia** **(** Lead Developer **)**
        - Python
        - C#
        - Core Java
        - JSON
        - PHP
        - .NET Framework
        - Xamarin Framework
        - Web Development
        - Google Accessibility Packages


- ### Website Designer
    - **Shivangi Mittal**
        - HTML
        - CSS
        - Bootstrap
        - Website Designing

## 7.  Team
### Team Name : MadTitans
- #### [Dhruv Kanojia](https://bitbucket.org/Xonshiz/) (Lead Developer)
- #### [Ankit Pass](https://bitbucket.org/ankitpassi141/) (UI/UX Designer)
- #### [Shivangi Mittal](https://bitbucket.org/Shivangi48/) (Website Designer)