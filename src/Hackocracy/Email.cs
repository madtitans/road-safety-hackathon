using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Hackocracy
{
    public class Email
    {
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string BId { get; set; }
        public string currentNgoID { get; set; }
        public string ImageUrl { get; set; }
        public bool Favorite { get; set; }
        public ImageView Photo { get; set; }
    }
}