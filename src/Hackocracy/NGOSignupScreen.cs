using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Graphics;

namespace Hackocracy
{
    [Activity(Label = "NGOSignupScreen", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class NGOSignupScreen : AppCompatActivity
    {
        public string ngoNameF, ngoRegistrationIdF, ngophoneNumberF, ngoemailF, ngoaddressF, ngopasswordF, message;
        public bool signupStatus;

        private ProgressBar spinner;

        EditText SignupNGOName;
        EditText SignupNGORegistrationID;
        EditText SignupNGOPhoneNumber;
        EditText SignupNGOAddress;
        EditText SignupNGOEmailID;
        EditText SignupNGOPassword;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NGOSignupScreen);

            var SignupButton = FindViewById<Button>(Resource.Id.NGOSignupButtonProceed);
            SignupNGOName = FindViewById<EditText>(Resource.Id.NGORegistrationName);
            SignupNGORegistrationID = FindViewById<EditText>(Resource.Id.NGORegistrationID);
            SignupNGOPhoneNumber = FindViewById<EditText>(Resource.Id.NGOSignupPhoneNumber);
            SignupNGOAddress = FindViewById<EditText>(Resource.Id.NGOSignupAddress);
            SignupNGOEmailID = FindViewById<EditText>(Resource.Id.NGOSignupEmailID);
            SignupNGOPassword = FindViewById<EditText>(Resource.Id.NGOSignupPassword);
            spinner = FindViewById<ProgressBar>(Resource.Id.circleProgress);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            var hashTag = FindViewById<TextView>(Resource.Id.textViewHashtagNGO2);
            hashTag.SetTypeface(tf, TypefaceStyle.Normal);
            SignupButton.SetTypeface(tf, TypefaceStyle.Bold);
            SignupButton.Click += SignupButton_ClickAsync;
        }

        private async void SignupButton_ClickAsync(object sender, EventArgs e)
        {
            //var IntentLoginButton = new Intent(this, typeof(NGOMainScreen));
            //StartActivity(IntentLoginButton);

            if (String.IsNullOrEmpty(SignupNGOName.Text) | String.IsNullOrEmpty(SignupNGORegistrationID.Text) | String.IsNullOrEmpty(SignupNGOPhoneNumber.Text) | String.IsNullOrEmpty(SignupNGOAddress.Text) | String.IsNullOrEmpty(SignupNGOEmailID.Text) | String.IsNullOrEmpty(SignupNGOPassword.Text))
            {
                Toast.MakeText(this, "Fields Cannot Be Empty.", ToastLength.Long).Show(); // Empty fields error.
            }
            else
            {
                if (Android.Util.Patterns.EmailAddress.Matcher(SignupNGOEmailID.Text).Matches())
                {
                    spinner.Visibility = ViewStates.Visible;

                    await Task.Run(() => SignUpPoster(SignupNGOName.Text, SignupNGORegistrationID.Text, SignupNGOPhoneNumber.Text, SignupNGOAddress.Text, SignupNGOEmailID.Text, SignupNGOPassword.Text));


                    if (this.signupStatus == true)
                    {
                        // Upon successful sign up, we move forward to next screen!
                        var IntentLoginButton = new Intent(this, typeof(NGOMainScreen));
                        StartActivity(IntentLoginButton);
                        IntentLoginButton.PutExtra("ngoIDRegistered", ngoRegistrationIdF);
                        Finish();
                    }
                    else
                    {
                        Toast.MakeText(this, this.message, ToastLength.Long).Show(); //Showing Bad Connection Error
                    }

                    spinner.Visibility = ViewStates.Gone;
                }
                else
                {
                    Toast.MakeText(this, "Please Enter A Valid Email Address.", ToastLength.Long).Show();
                }
            }
        }

        private async Task SignUpPoster(string ngoName, string ngoRegistrationId, string ngophoneNumber, string ngoemail, string ngoaddress, string ngopassword)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://xonshiz.heliohost.org");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                // "+91-" to make the phone number in correct format
                new KeyValuePair<string, string>("ngoName", ngoName),
                new KeyValuePair<string, string>("ngoRegistrationId", ngoRegistrationId),
                new KeyValuePair<string, string>("userphoneNumber", "+91-" + ngophoneNumber),
                new KeyValuePair<string, string>("useremail", ngoaddress),
                new KeyValuePair<string, string>("useraddress", ngoemail),
                new KeyValuePair<string, string>("userpassword", ngopassword)

            });
                var result = await client.PostAsync("/hackoracy/api/NGORegistration/sign_up.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                        this.signupStatus = false; // Bad Input?
                    }
                    else
                    {
                        this.ngoNameF = obj2.ngoName;
                        this.ngoRegistrationIdF = obj2.ngoRegistrationId;
                        this.ngophoneNumberF = obj2.ngoPhoneNumber;
                        this.ngoemailF = obj2.ngoEmail;
                        this.ngoaddressF = obj2.ngoAddress;
                        this.signupStatus = true; // Flag to tell the other methods about Sign up status
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }
    }
}