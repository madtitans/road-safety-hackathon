
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hackocracy
{

    [Activity(Label = "Raise a New Issue", Theme = "@style/Theme.AppCompat.Light.DarkActionBar")]
    public class BeggerMainScreen : AppCompatActivity
    {
        ImageView imageView;
        Button submitRecord;
        EditText beggarAddress, beggarExtraInfo;
        BottomNavigationView bottomNavigationButton;

        public string usersAdhar, photoData, message;
        public bool flag;

        protected override void OnCreate(Bundle bundle)
        {
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.BeggerMainScreenListView);

            usersAdhar = Intent.GetStringExtra("userAdharNo") ?? "000000000000";

            var CameraButton = FindViewById<ImageButton>(Resource.Id.beggerCameraButton);
            submitRecord = FindViewById<Button>(Resource.Id.BeggerSubmitButton);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            submitRecord.SetTypeface(tf, TypefaceStyle.Normal);
            imageView = FindViewById<ImageView>(Resource.Id.beggerCameraPictureRecord);
            beggarAddress = FindViewById<EditText>(Resource.Id.editText2);
            beggarExtraInfo = FindViewById<EditText>(Resource.Id.editText3);
            //bottomNavigationButton = FindViewById<BottomNavigationView>(Resource.Id.bottom_navigation1);
            CameraButton.Click += CameraButton_Click;
            submitRecord.Click += RecordSubmitAsync;
            //bottomNavigationButton.NavigationItemSelected += BottomNavigationButton_NavigationItemSelected;
        }

        //private void BottomNavigationButton_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        //{
        //    switch(e.Item.ItemId)
        //    {
        //        case Resource.Id.action_submit:
        //            bottomNavigationButton.Click += RecordSubmitAsync;
        //            break;
        //    }
        //}

        private async Task SignInPoster(string address, string extrainformation)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://xonshiz.heliohost.org");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                // "+91-" to make the phone number in correct format
                new KeyValuePair<string, string>("photoLink", photoData),
                new KeyValuePair<string, string>("address", address),
                new KeyValuePair<string, string>("cordinates", "000,000"),
                new KeyValuePair<string, string>("extraInformation", extrainformation),
                new KeyValuePair<string, string>("addedBy", usersAdhar),
                new KeyValuePair<string, string>("status", "New"),
                new KeyValuePair<string, string>("ngoInfo", "000000")

            });
                var result = await client.PostAsync("/hackoracy/api/beggerInfoInsert.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                        this.flag = false;
                    }
                    else
                    {
                        this.message = "Successfully Added";
                        this.flag = true;
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }

        private void CameraButton_Click(object sender, System.EventArgs e)
        {
            Intent cameraIntent = new Intent(MediaStore.ActionImageCapture);
            //Intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(App._file));
            StartActivityForResult(cameraIntent, 0);
        }

        private async void RecordSubmitAsync(object sender, System.EventArgs e)
        {
            if (!String.IsNullOrEmpty(beggarAddress.Text))
            {
                await Task.Run(() => SignInPoster(beggarAddress.Text, beggarExtraInfo.Text));
                //Toast.MakeText(this, "Done : " + message, ToastLength.Long).Show();
                if (this.flag)
                {
                    beggarAddress.Text = "";
                    beggarExtraInfo.Text = "";
                    //base.OnRestart();
                    imageView.SetImageDrawable(null);
                    Toast.MakeText(this, "Added Successfully", ToastLength.Long).Show();
                }
                else
                {
                    Toast.MakeText(this, "Some Error Occured. Try again later.", ToastLength.Long).Show();
                    Toast.MakeText(this, this.message, ToastLength.Long).Show();
                }
            }
            else
            {
                Toast.MakeText(this, "Address Is Mandatory.", ToastLength.Long).Show();
            }
        }

        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            Toast.MakeText(this, "Action Not Allowed!", ToastLength.Long).Show();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);


            Bitmap bitmap = (Android.Graphics.Bitmap)data.Extras.Get("data");

            using (var stream = new System.IO.MemoryStream())
            {
                bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Webp, 100, stream);
                byte[] imageBytes = stream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                Console.WriteLine("Base 64 String : " + base64String);
                photoData = base64String;
                //inst.saveImage(base64String);
            }
            imageView.SetImageBitmap(bitmap);

            //Bitmap cameraBitmap = (Bitmap)data.Extras.Get("data");
            //byte[] bitmapData;
            //using (var stream = new MemoryStream())
            //{
            //    cameraBitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
            //    bitmapData = stream.ToArray();
            //    Console.WriteLine("This is bitmapData : " + bitmapData);
            //    Bitmap bitmap = BitmapFactory.DecodeByteArray(bitmapData, 0, bitmapData.Length);
            //    Console.WriteLine("This is bitmap : " + bitmap);
            //}
            //Console.WriteLine("This is Data : " + data);
            //Console.WriteLine("This is Data Extrasg : " + data.Extras);
            //Console.WriteLine("This is Bitmap : " + cameraBitmap);

            //imageView.SetImageBitmap(cameraBitmap);

        }


    }
}