using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hackocracy
{
    [Activity(Label = "Hackocracy", Icon = "@drawable/icon", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class NGOLoginScreen : AppCompatActivity
    {
        public string ngoName, ngoRegistrationId, ngoPhoneNumber, ngoEmail, ngoAddress, message;
        public bool signinStatus;

        private ProgressBar spinner;

        EditText NGORegistrationID;
        EditText NGOPassword;

        protected override void OnCreate(Bundle bundle)
        {
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.NGOLoginScreen);

            var LoginButton = FindViewById<Button>(Resource.Id.NGOLoginButton);
            var SignupButton = FindViewById<Button>(Resource.Id.NGOSignupButton);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            var hashTag = FindViewById<TextView>(Resource.Id.textViewHashtagNGO);
            hashTag.SetTypeface(tf, TypefaceStyle.Normal);
            NGORegistrationID = FindViewById<EditText>(Resource.Id.loginRegistrationID);
            NGOPassword = FindViewById<EditText>(Resource.Id.loginNGOPassword);
            spinner = FindViewById<ProgressBar>(Resource.Id.circleProgress);
            LoginButton.SetTypeface(tf, TypefaceStyle.Normal);
            SignupButton.SetTypeface(tf, TypefaceStyle.Normal);
            LoginButton.Click += LoginButton_ClickAsync;
            SignupButton.Click += SignupButton_Click;
        }

        private void SignupButton_Click(object sender, System.EventArgs e)
        {
            var IntentSignupButton = new Intent(this, typeof(NGOSignupScreen));
            StartActivity(IntentSignupButton);
        }

        private async void LoginButton_ClickAsync(object sender, System.EventArgs e)
        {
            //var IntentLoginButton = new Intent(this, typeof(NGOMainScreen));
            //StartActivity(IntentLoginButton);
            if (String.IsNullOrEmpty(NGORegistrationID.Text) | String.IsNullOrEmpty(NGOPassword.Text))
            {
                Toast.MakeText(this, "Fields Cannot Be Empty.", ToastLength.Long).Show(); // Empty fields error.
            }
            else
            {
                spinner.Visibility = ViewStates.Visible;

                await Task.Run(() => SignInPoster(NGORegistrationID.Text, NGOPassword.Text));


                if (this.signinStatus == true)
                {
                    // Upon successful sign up, we move forward to next screen!
                    var IntentLoginButton = new Intent(this, typeof(NGOMainScreen));
                    IntentLoginButton.PutExtra("ngoIDRegistered", ngoRegistrationId);
                    StartActivity(IntentLoginButton);
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, this.message, ToastLength.Long).Show(); //Showing Bad Connection Error
                }

                spinner.Visibility = ViewStates.Gone;
            }
        }

        private async Task SignInPoster(string ngoRegistrationId, string password)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://xonshiz.heliohost.org");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                new KeyValuePair<string, string>("ngoRegistrationId", ngoRegistrationId),
                new KeyValuePair<string, string>("password", password)

            });
                var result = await client.PostAsync("/hackoracy/api/NGORegistration/sign_in.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                        this.signinStatus = false; // Bad Input?
                    }
                    else
                    {
                        
                        this.ngoName = obj2.ngoName;
                        this.ngoRegistrationId = obj2.ngoRegistrationId;
                        this.ngoPhoneNumber = obj2.ngoPhoneNumber;
                        this.ngoEmail = obj2.ngoEmail;
                        this.ngoAddress = obj2.ngoAddress;
                        this.signinStatus = true; // Flag to tell the other methods about Sign up status
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }
    }
}